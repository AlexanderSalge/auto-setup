#!/bin/bash

echo "Setting up the ssh key ..."

ssh-keygen -t rsa -b 2048 -C "AI4CE DevBox"

echo "ssh key keypair created. Please copy the public key from '~/.ssh/' to your GitLab settings."


# Ask the user for their name
echo "Please enter your name:"

# Read the input from the user and assign it to the variable 'username'
read username


# Print a message using the user's input
echo "Hello, $username. Nice to meet you."

echo "Please enter your email:"
read usermail

git config --global user.name "$username"
git config --global user.email "$usermail"
echo "Thank you, $username. Your git setup should be good to go!"
