# auto-setup

To easily onboard new team members to the AI4CE project, this repo holds automated tools to setup the work environment. 

# Ingredients

The proposed setup for the AI4CE project contains:
- VM image running Linux Lite
- VS Codium 
- git 

# Get Started with the AI4CE DevBox
1. Install VirutalBox + Extension Pack [Oracle Website](https://www.virtualbox.org/wiki/Downloads) \
**Hint:** Windows User might need the `Visual Studio C++ Redistributable Package` [Microsoft Website](https://learn.microsoft.com/en-us/cpp/windows/latest-supported-vc-redist?view=msvc-170)
2. Get `.ova` VM image from Jan-Peter
3. Import VM by double clicking


# Get Started with the development
1. Create SSH Key and add to your GitLab profile [GitLab Tutorial](https://docs.gitlab.com/ee/user/ssh.html)
- `ssh-keygen -t rsa -b 2048 -C "<comment>"`
- in `~/.ssh/` copy contenct of the `.pub` file to Profile -> Preferences -> SSH in GitLab
2. git clone <project URL>
3. cd <project name>
4. poetry shell
5. poetry Install
6. pre-commit install


# Setup Software

Not decided yet
- [ ] Option1: [Vagrant](https://github.com/hashicorp/vagrant)
- [ ] Option2: plane [Ansible](https://github.com/ansible/ansible)
